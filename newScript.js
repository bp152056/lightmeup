var points = []; //stores points which will be used to connect our lines
var freq = [];
var prevPoints = [
    []
]; //stores our previous points so we can draw a trail

var songs = []; //stores our different songs
var currentSong = 2;

var particles = [];
var maxParticles = 600;

var screen_width = 1200;
var screen_height = 1000;
const X_OFFSET = 100;

var standard = true;

var circular = false;
var radiusIncrease = 250;
var r = 100;

var globalHue = 0;
var saturation = 128;


var beatLow, beatLowMid, beatMid;

function preload() {
    songs[0] = loadSound('VULFPECK-Back_Pocket.mp3');
    songs[1] = loadSound('Kaytranada-Janet-Jackson-remix.mp3');
    songs[2] = loadSound('Maceo-Plex-Sparks-Of-Life.mp3');
    songs[3] = loadSound('Teedra-Moses-Be-Your-Girl(Kaytranada-Edition).mp3');
    songs[4] = loadSound('Robert-Hood-Dancer(Original-Mix).mp3');
}

function setup() {
    createCanvas(screen_width, screen_height);
    colorMode(HSB);
    angleMode(DEGREES);

    songs[currentSong].play();
    console.log(songs);
    freq = new p5.FFT(0.8, 512);


    beatLow = new BeatDetect(80, 120, "bass", 0.7);
    beatLowMid = new BeatDetect(140, 400, "lowMid", 0.7);
    beatMid = new BeatDetect(400, 2600, "Mid", 0.7);

}


function draw() {

    background(0);
    var spectrum = freq.analyze(); // stores fft into array

    if (globalHue == 255) {
        globalHue = 0;
    } else globalHue++;


    updatepoints(spectrum);
    drawFrequency(spectrum);
    updateBeats(spectrum);


}


function drawFrequency(spectrum) {

    noFill();
    push();

    if (standard) {
        translate(X_OFFSET, height / 2);

        for (var i = 0; i < (points.length / 2) - 1; i++) {

            if (i > 1) {

                var d = abs(dist(points[i].pos.x, points[i].pos.y,
                    points[i].pos.x, 0));

                stroke(globalHue, 100, 255);
                fill(globalHue, saturation, 150)
                strokeWeight(constrain(d * 0.05, 1, 5));

                beginShape();
                curveVertex(points[i - 2].pos.x, points[i - 2].pos.y);
                curveVertex(points[i - 1].pos.x, points[i - 1].pos.y);
                curveVertex(points[i].pos.x, points[i].pos.y);
                curveVertex(points[i + 1].pos.x, points[i + 1].pos.y);
                endShape();
            }
        }
        pop();

    } else {
        push();
        translate(width / 2, height / 2);

        for (var i = 0; i <= 360; i++) {

            if (i > 1) {
                var d = abs(dist(points[i].pos.x, points[i].pos.y,
                    points[i].pos.x, 0));

                stroke(globalHue, d * 10, 255);
                strokeWeight(constrain(d * 0.1, 1, 5));



                beginShape();
                curveVertex(points[i - 2].pos.x, points[i - 2].pos.y);
                curveVertex(points[i - 1].pos.x, points[i - 1].pos.y);
                curveVertex(points[i].pos.x, points[i].pos.y);
                curveVertex(points[i + 1].pos.x, points[i + 1].pos.y);

                endShape();

            }
        }
        pop();
    }


}

function drawBeats(bass, treble, mid) {

    var minrad = 15;
    var maxrad = 250;

    var bassRadius = map(bass, 0, 255, minrad, maxrad);
    var trebleRadius = map(treble, 0, 255, minrad, maxrad);
    var midRadius = map(mid, 0, 255, minrad, maxrad);

    noStroke();
    fill(globalHue, bass, bass);
    ellipse(width / 6, 3 * height / 4, bassRadius, bassRadius);

    fill(globalHue, mid, mid);
    ellipse(3 * width / 6, 3 * height / 4, midRadius, midRadius);


    fill(globalHue, treble, treble);
    ellipse(5 * width / 6, 3 * height / 4, trebleRadius, trebleRadius);

}

function updatepoints(spectrum) {

    for (var i = 0; i < spectrum.length; i++) {

        var y = map(spectrum[i], 0, 255, 0, -height / 2);
        var r_plus = map(y, 0, -height / 2, 0, radiusIncrease);

        if (standard) {
            var x = map(i, 0, spectrum.length / 2, 0, width - 2 * X_OFFSET);

        } else {
            var rad = r + r_plus;
            var x = rad * cos(i);
            y = rad * sin(i);
        }

        points[i] = new Point(x, y);
    }


}

function updateBeats(spectrum) {


    translate(0, 0);

    //gets various energies for lows mids and highs
    /* var bass    = freq.getEnergy( "bass" );
     var treble  = freq.getEnergy( "treble" );
     var mid     = freq.getEnergy( "mid" ); 
     */

    //drawBeats(bass,treble,mid);



    beatLow.update(freq);
    beatLowMid.update(freq);
    beatMid.update(freq);

    stroke(255);
    strokeWeight(1);
    var h = map(beatLow.treshold, 0, 1, height / 2, 0);
    line(0, h, width, h);

    if (beatLow.isDetected == true) {
        //sends http request to server to turn on dmx console
        sendMessage();
        
        for (var i = 0; i < 15; i++) {
            if (standard) {
                particles.push(new Particle(width / 6, 3 * height / 4));
            } else {
                particles.push(new Particle(width / 2, height / 2));
            }

        }
    }

    if (standard) {
        beatLow.display(width / 6, 3 * height / 4, globalHue);
        beatLowMid.display(3 * width / 6, 3 * height / 4, globalHue);
        beatMid.display(5 * width / 6, 3 * height / 4, globalHue);
    } else {
        beatLow.display(width / 2, height / 2, globalHue)
    }

    updateParticles();
}

function updateParticles() {

    for (var i = 0; i < particles.length; i++) {
        particles[i].update();
        particles[i].show();
    }
    for (i = 0; i < particles.length; i++) {
        if (particles[i].dead) {
            particles.splice(i, 1);
        }
    }
}

function sendMessage() {
    const Http = new XMLHttpRequest();
    const url = 'http://localhost:8081';
    Http.open("GET", url);
    Http.send();

}


function keyPressed() {
    //spacebar
    console.log(keyCode);
    if (keyCode == 32) {
        if (songs[currentSong].isPaused()) {
            songs[currentSong].play();
        } else songs[currentSong].pause();
    }
    //right arrow
    if (keyCode == 39) {
        if (currentSong < songs.length - 1) {
            songs[currentSong].pause();
            currentSong++;
            songs[currentSong].play();
        }

    }
    //left arrow
    if (keyCode == 37) {
        if (currentSong > 0) {
            songs[currentSong].pause();
            currentSong--;
            songs[currentSong].play();
        }

    }
    //for circular display
    //key ^pressed 'c'
    if (keyCode == 67) {
        console.log("circular");
        standard = false;
        circular = true;
    }
    //standard display (linear)
    //key pressed 'l'
    if (keyCode == 76) {
        console.log("linear");
        standard = true;
        circular = false;
    }

}