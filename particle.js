
var initSpeed = 5;

function Particle(x, y) {
    //vectors that will be used by our particles
    this.pos = createVector(x - 25 + Math.random() * 50, y - 25 + Math.random() * 50); //position
    this.vel = createVector(Math.random() * (2*initSpeed) - initSpeed, Math.random() * (2*initSpeed) - initSpeed); //velocity
    this.acc = createVector(0, 0); //acceleration

    this.life = 1 + Math.random() * 5;
    this.dead = false;


    this.show = function () {

        noStroke();
        fill(128, 255, 50);
        //stroke(200);
        ellipse(this.pos.x, this.pos.y, Math.ceil(this.life), Math.ceil(this.life));
    };

    this.update = function () {
        this.life -= 0.1; //decrease the size of the particle
        if (this.life < 0) {
            this.dead = true;
        }
        this.vel.add(this.acc);
        this.pos.add(this.vel);
    };
}