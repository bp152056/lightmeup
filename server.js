// HTTP Portion
var http = require('http');
// Path module
var path = require('path');

//dmx node module
var DMX = require('dmx');

// Using the filesystem module
var fs = require('fs');

var server = http.createServer(handleRequest);
server.listen(8080);

console.log('Server started on port 8080');

function handleRequest(req, res) {
  // What did we request?
  var pathname = req.url;

  // If blank let's ask for index.html
  if (pathname == '/') {
    pathname = '/index.html';
  }

  // Ok what's our file extension
  var ext = path.extname(pathname);

  // Map extension to file type
  var typeExt = {
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.css': 'text/css',
    '.mp3': 'audio/mpeg'

  };
  // What is it?  Default to plain text

  var contentType = typeExt[ext] || 'text/plain';

  // User file system module
  if (ext === ".mp3") {

    var filepath = path.join(__dirname, pathname);
    //var stat = fs.statSync(filepath);

    res.writeHead(200, {
      'Content-Type': 'audio/mpeg'
    });

    var readStream = fs.createReadStream(filepath);
    // We replaced all the event handlers with a simple call to readStream.pipe()
    readStream.pipe(res);

  } else {

    //not an mp3 file 
    fs.readFile(__dirname + pathname,
      // Callback function for reading
      function (err, data) {
        console.log(__dirname + pathname);
        // if there is an error
        if (err) {

          res.writeHead(500);
          return res.end('Error loading ' + pathname);
        }
        // Otherwise, send the data, the contents of the file

        res.writeHead(200, {
          'Content-Type': contentType
        });
        res.end(data);
      }
    );
  }


}


/////////////// serve to listen for beats
var on = false;
var beatServer = http.createServer(beatListener)
beatServer.listen(8081);

function beatListener(req, res) {
  //we have received a request
  //console.log("beat received");
  switchLight();
  res.end();
}

///////////////////////////////////////////////////////

//dmx setup

var A = DMX.Animation;

var dmx = new DMX();
var universe = dmx.addUniverse('demo', 'enttec-usb-dmx-pro', '/dev/cu.usbserial-EN172871');

function switchLight() {
  /*on ? universe.updateAll(0) : universe.updateAll(255);
  console.log(on ? "light on" : "light off");
  on = !on;
  */


  universe.updateAll(200);
  //turn off the lights after 500 ms
  setTimeout(function () {
    universe.updateAll(0);
  }, 100);
}

var on = false;