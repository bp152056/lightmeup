var threshold = 500;// threshold for sound to create particles
const MAX_PARTICLES =1000;
const X_OFFSET = 100; 
var soundFile;
var freq = [];
var screen_width = 800;
var screen_height = 600;
var particles = [];
var prevSpectrums =[];


function preload() {
    soundFile = loadSound('VULFPECK-Back_Pocket.mp3');

}

function setup() {
    createCanvas(screen_width, screen_height);
    colorMode(HSB,255);

    freq = new p5.FFT(0.9, 256); // fft with 256 bins and a smoothing of 0.9
    soundFile.setVolume(0.1);
    soundFile.play();
}

function draw() {

    background(0);
    
    drawSoundCurve();
    //update the various particles
    updateParticles();
    

}

function updateParticles() {
    for (var i = 0; i < particles.length; i++) {

        particles[i].update();
        particles[i].show();

    }
    for ( i = 0; i < particles.length; i++) {
        if (particles[i].dead) {
            particles.splice(i, 1);
        }
    }
}

function drawSoundCurve(){
    
    var spectrum = freq.analyze(); //store the fft into an array for the spectrum (256 cases)
    var ampSum =0;

    //change of base, use translate function

    //draw various frequency parts
    translate(X_OFFSET,height/2);
    beginShape();
    //strokeWeight(1);
    //stroke(255);
    noFill();

    for (var i = 0; i < spectrum.length/2; i++) {
  
        var x = map(i, 0, spectrum.length/2, 0, width - 2*X_OFFSET);
        var h = map(spectrum[i], 0, 255, 0, -height/2);
        ampSum += h;
        var weight =  map (spectrum[i], 0, 255,1,4);
        
        var col = map(i, 0, spectrum.length/2,0,255);
        stroke(col,col,255);
        strokeWeight(weight);
        curveVertex(x, h);
    }
    endShape();
    updateThreshold(ampSum/spectrum.length/2);
    pop();

    drawTrail(spectrum);
    
}

function drawTrail(spec){
    
    prevSpectrums.push(spec);

    if(prevSpectrums.length > 10){
        prevSpectrums = prevSpectrums.slice(1,10);
    }

    for(var j=0;j<prevSpectrums.length;j++){
        translate(X_OFFSET,height/2);
        var alpha = map(j,0,prevSpectrums.length,255,0);
        
        spectrum = prevSpectrums[j];
        stroke(100,255,alpha);
        beginShape();
        //strokeWeight(1);
        //stroke(255);
        noFill();

        for (var i = 0; i < spectrum.length/2; i++) {
    
            var x = map(i, 0, spectrum.length/2, 0, width - 2*X_OFFSET);
            var h = map(spectrum[i], 0, 255, 0, -height/2);
            ampSum += h;
            
            curveVertex(x, h);
        }
        endShape();
        updateThreshold(ampSum/spectrum.length/2);
        pop();
    }
}

function updateThreshold(avgLevel){
    //avg threshold can now be set and decremented slowly 
    threshold = avgLevel;
    /*
    stroke(255,);
    strokeWeight(1);
    line(0,threshold,width,threshold);
    */
}

function keyPressed() {
    //spacebar
    if (keyCode == 32) {
        if (soundFile.isPaused()) {
            soundFile.play();
        } else soundFile.pause();
    }
}

function mousePressed() {

    if (particles.length < 255) {
        particles.push(new Particle(mouseX, mouseY));
    }

}