function BeatDetect(f1,f2,str,thresh){
    this.isDetected = false;
    this.f1=f1;
    this.f2=f2;
    this.str = str;
    this.treshold = thresh;
    this.energy = 0;

    this.siz = 10;
    this.sensitivity = 100;
}

BeatDetect.prototype.display = function(x,y,hue) {
    var bright = 100;
    if(this.isDetected == true){
        bright = 255
        this.siz = lerp(this.siz,60,0.99);
    }
    else if (this.isDetected == false){
        this.siz = lerp(this.siz,20,0.99);
    }
    noStroke();
    fill(hue%255,100,bright);
    ellipse(x,y,this.siz,this.siz);
    //fill(0);
    //text(this.str,x,y);
    //text("( "+this.f1+" - "+this.f2+"Hz )",x,y+10);
    
}

BeatDetect.prototype.update = function(fftObject) {
    this.energy = fftObject.getEnergy(this.f1,this.f2)/255;

    if(this.isDetected == false){
        if (this.energy > this.treshold){
            this.treshold += (this.energy-this.treshold) ;
            this.isDetected = true;
            var self = this;
            //sets the detected value to false after a certain time interval
            setTimeout(function () {
                self.isDetected = false;
            },this.sensitivity);
        }else{
            //slowly decrese threshold to readjust for levels
            if(this.treshold > 0.01){
                this.treshold -= 0.001;
            }
            
        }
    }   
}